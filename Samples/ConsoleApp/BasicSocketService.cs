﻿using System;
using System.Net.WebSockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Fleck.Samples.ConsoleApp
{
    class BasicSocketService : IBasicSocketService
    {
        string _username;
        ClientWebSocket _webSocket;

        public async Task Connect(ClientWebSocket webSocket, string username)
        {
            _username = username;
            _webSocket = webSocket;

            //Connect to socket
            var connection = webSocket.ConnectAsync(
              new Uri("ws://127.0.0.1:8181"),
              CancellationToken.None);

            await Task.WhenAll(Receive(), Send());
        }

        public async Task Receive( )
        {
            var buffer = new ArraySegment<byte>(new byte[2048]);

            //Wait untill socket connected
            while (_webSocket.State != WebSocketState.Open) { }

            //send the "connected" message
            await _webSocket.SendAsync(
                new ArraySegment<byte>(Encoding.ASCII.GetBytes(new Message(_username, "CONECTADO").ToString())),
                WebSocketMessageType.Text,
                true,
                CancellationToken.None);

            //Receiving messages while socket is open
            while (_webSocket.State == WebSocketState.Open)
            {
                WebSocketReceiveResult result = await _webSocket.ReceiveAsync(buffer, CancellationToken.None);

                string message = Encoding.ASCII.GetString(buffer.Array,
                     buffer.Offset, result.Count);

                Console.WriteLine(message);
            }
        }
        public async Task Send( )
        {
            var input = Console.ReadLine();
            //Send messsages until /quit or /q is written 
            while (input != "/quit" && input != "/q" )
            {
                if (_webSocket.State == WebSocketState.CloseReceived)
                    return;

                Message messageToSend = new Message(_username, input);

                await _webSocket.SendAsync(
                  new ArraySegment<byte>(Encoding.ASCII.GetBytes(messageToSend.ToString())),
                  WebSocketMessageType.Text,
                  true,
                  CancellationToken.None);

                input = Console.ReadLine();
            }

            //send the "disconnected" message
            await _webSocket.SendAsync(
                new ArraySegment<byte>(Encoding.ASCII.GetBytes(new Message(_username, "DESCONECTADO").ToString())),
                WebSocketMessageType.Text,
                true,
                CancellationToken.None);

            //Close the socket
            await _webSocket.CloseAsync(
                   WebSocketCloseStatus.NormalClosure,
                   String.Empty,
                   CancellationToken.None);
        }
    }
}
