﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;

namespace Fleck.Samples.ConsoleApp
{
    class ChatFleck
    {
        static int Main(string[] args)
        {
            var allSockets = new List<IWebSocketConnection>();
            string username;
            bool isServer = false;

            if (args.Length != 1)
            {
                Console.WriteLine("Se necesita como único argumento el número de puerto a utilizar. Ej: 8181");
                Console.ReadLine();
                return -1;
            }
            Console.WriteLine("¿Cuál es tu nombre?");
            username = Console.ReadLine();
            
            var uri = new Uri("ws://127.0.0.1:"+args[0]);
            IPAddress _locationIP = IPAddress.Parse(uri.Host);
            var _connectingSocket = new Socket(_locationIP.AddressFamily, SocketType.Stream, ProtocolType.IP);
            
            //Try to connect to socket, if fails, server is launched
            try
            {
                _connectingSocket.Connect("localhost", 8181);
            }
            catch 
            {
                isServer = true;
            }

            if(isServer)
            {
                var server = new WebSocketServer("ws://0.0.0.0:" + args[0]);
                
                //New server started
                server.Start(socket =>
                {
                    socket.OnOpen = () =>
                    {
                        Console.WriteLine("Usuario conectado");
                        allSockets.Add(socket);
                    };
                    socket.OnClose = () =>
                    {
                        Console.WriteLine("Usuario desconectado");
                        allSockets.Remove(socket);
                    };
                    socket.OnMessage = message =>
                    {
                        Console.WriteLine(message);
                        //Resend the message to all sockets except to origin
                        foreach (var s in allSockets.ToList())
                        {
                            if (socket.ConnectionInfo.Id != s.ConnectionInfo.Id)
                                s.Send(message.ToString());
                        }
                    };
                });
                Console.WriteLine("Servidor iniciado");
                
                var input = Console.ReadLine();
                //Send messsages until /quit or /q is written
                while (input != "/quit" && input != "/q")
                {
                    Message messageToSend = new Message(username, input);

                    foreach (var socket in allSockets.ToList())
                    {
                        socket.Send(messageToSend.ToString());
                    }
                    input = Console.ReadLine();
                }
                //Close all the sockets
                foreach (var socket in allSockets.ToList())
                {
                    socket.Close();
                }
            }
            else
            {
                //Init the client
                IBasicSocketService basicSocketService = new BasicSocketService();
                WebSocketClient webSocketClient = new WebSocketClient(basicSocketService, username);
                webSocketClient.Start();
            }
            return 0;
        }
    }
}
