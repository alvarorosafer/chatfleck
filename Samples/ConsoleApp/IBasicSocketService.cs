﻿using System;
using System.Net.WebSockets;
using System.Threading.Tasks;

namespace Fleck.Samples.ConsoleApp
{
    public struct Message
    {
        readonly string username;
        readonly DateTime date;
        readonly string message;

        public Message(string username, string message)
        {
            this.username = username;
            this.date = DateTime.Now;
            this.message = message;
        }

        public override string ToString()
        {
            return date + " " + username + ": " + message;
        }
    }
    public interface IBasicSocketService
    {
        Task Connect(ClientWebSocket ws, string username);
        Task Receive();
        Task Send();
    }

}
