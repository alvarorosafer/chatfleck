﻿using System;
using Fleck.Samples.ConsoleApp;
using System.Net.WebSockets;
using System.Threading;

namespace Fleck
{
    public class WebSocketClient
    {
        IBasicSocketService _basicSocketService;
        string _username;
        
        public WebSocketClient(IBasicSocketService basicSocketService, string username)
        {
            _basicSocketService = basicSocketService;
            _username = username;
        }

        public void Start()
        {
            var websocketClient = new ClientWebSocket();

            //Connects client by the injected interface
            _basicSocketService.Connect(websocketClient, _username).Wait();
        }

    }
}

