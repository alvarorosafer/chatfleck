El ejercicio est� resuelto en el proyecto "ConsoleApp" dentro de la carpeta Samples. 

El ejecutable se puede lanzar desde Samples/ConsoleApp/bin/Release/Fleck.Samples.ConsoleApp.exe, con un �nico argumento: el puerto a conectar.

El programa pedir� un nombre de usuario nada m�s comenzar. Cuando haya dos o m�s usuarios conectados el chat enviar� los mensajes que se deseen a los dem�s usuarios.

El programa saldr� de cada ejecuci�n cuando se introduzca el comando /q o /quit.


Implementaci�n:

El programa est� implementado con tres clases y una interfaz:

	- ChatFleck.cs contiene el programa Main. Recibe el puerto a conectar y si es el primero en llegar se conecta como servidor, si no, como cliente.
		El servidor est� implementado mediante la clase WebSocketServer de Fleck y b�sicamente funciona con los eventos OnOpen, OnClose y OnMessage.
		El cliente est� implementado en la clase WebSocketClient, a la que se le inyecta mediante el constructor la interfaz IBasicSocketService.

	- WebSocketClient.cs recibe un nombre de usuario y una interfaz IBasicSocketService y le pide que empiece la conexi�n.

	- BasicSocketService.cs implementa la interfaz IBasicSocketService y se encarga de conectar, cerrar conexi�n y enviar y recibir mensajes as�ncronamente mediante tasks.

	- IBasicSocketService.cs interfaz para el uso as�ncrono de sockets. Contiene adem�s la estructura Message, que es el mensaje construido por: la fecha en la fue escrito,
		el nombre de usuario, y por �ltimo el mensaje que ha escrito en la consola. A esta estructura se le sobreescribe el m�todo ToString() para mandarlo completo con la funci�n Send.